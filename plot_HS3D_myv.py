##########################
##########################
# Tianmu Zhang, tzhang4@buffalo.edu
# Department of Material Design and Innovation
# https://engineering.buffalo.edu/materials-design-innovation.html
# University at Buffalo, The State University of New York
#
# Read the output file from Crystal Explorer, plot the Hirshfeld surface
# Mayavi2 is need for this code. Will implement the installation later.
import numpy as np
from mayavi import mlab
import HSXPLR as HSXPLR

# read the .cxs file, output file by Crystal Explorer.
def read_XPLR(file_name):
    h_surf = HSXPLR.HSXPLR(file_name+'.cxs')
    return h_surf

def plot_HS3D(file_name, fig_pop, resolution=20, surf_val = 'd_norm', msk_val=None):
    '''
    Plot the 3D Hirshfeld Surface.
    file_name, the .cxs file_name, not extesion needed.
    surf_val, the value used to color the H. surface.
    msk_val, masking value, will mask out the vertices with surf_val
    below is value. This will be used for filtration.
    '''
    if isinstance(file_name, str):
        h_surf = read_XPLR(file_name)
    elif isinstance(file_name, HSXPLR.HSXPLR):
        h_surf = file_name
    else:
        raise TypeError('Input is neither a HSXPLR object or a file name')
    x = np.array(h_surf.HSsf_vtx).T[0]
    y = np.array(h_surf.HSsf_vtx).T[1]
    z = np.array(h_surf.HSsf_vtx).T[2]

    vertices = h_surf.HSsf_vtxidx
    if surf_val == 'd_norm':
        sclrs = h_surf.VTX_dnorm
    elif surf_val == 'd_i':
        sclrs = h_surf.VTX_di
    elif surf_val == 'd_e':
        sclrs = h_surf.VTX_de
    else:
        raise TypeError('Using '+surf_val+' to color the surface is not\
                        currently supported.')

    if msk_val != None:
        # msk = np.array(sclrs)<=msk_val
        # x[np.array(sclrs)<=msk_val] = np.nan
        # y[np.array(sclrs)<=msk_val] = np.nan
        # z[np.array(sclrs)<=msk_val] = np.nan

        x[np.array(sclrs)>=msk_val] = np.nan
        y[np.array(sclrs)>=msk_val] = np.nan
        z[np.array(sclrs)>=msk_val] = np.nan

    else:
        msk = None
    # mlab.triangular_mesh(x, y, z, vertices, scalars=sclrs,
    #                     colormap='cool', mask=msk)
    hs = mlab.triangular_mesh(x, y, z, vertices, scalars=sclrs, #extent=extent,
                        # colormap='rainbow',
                        # colormap='RdBu',
                        colormap='coolwarm',
                        figure=fig_pop,
                        resolution=resolution,
                        )
    # mlab.view(azimuth=30, elevation=7, distance=40, focalpoint=(5,7,5))
    # mlab.show()
    return hs, mlab.view()
