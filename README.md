# Hirshfeld Surface Python Suit

### This NOT a software package for generating Hirshfeld Surface from .cif files.
### This is a python module which can be used to read the output files of Tonto, which is the backend of CrystalExplore.
### For more information about CrystalExplore and Tonto, please use the link https://crystalexplorer.scb.uwa.edu.au/.
### Hirshfeld Surface is a computational chemistry tool, which can be used to analyze crystalline materials, in particular the chemistry properties.

Currently the following properties of the Hirshfeld Surface is available, other properties will be added.  
* 3D coordinates of vertice of the surface
* triangulation of the surface using the vertice
* $'d_i'$ of the vertice
* $'d_e'$ of the vertice
* $'di_{norm}'$ of the vertice
* $'de_{norm}'$ of the vertice
* $'d_{norm}'$ of the vertice

Plotting and fingerprint generation modules are separated from the reading module.

The file plot_HS3D_myv.py is a plotting interface for Mayavi (https://docs.enthought.com/mayavi/mayavi/).
Support for Blender is planned.

### Demo 1, Material Barcode using Hirshfeld Surface

![Barcode on Hirshfeld Surface](demo/HS_flt_v2_2.mp4)
<!--<figure class="video_container">-->
  <!--<video controls="true" allowfullscreen="true" poster="path/to/poster_image.png">-->
<!--  <video height="240" controls>-->
<!--    <source src="demo/HS_flt_v2_2.mp4" type="video/mp4">-->
<!--  </video>-->
<!--</figure>-->

### Demo 2, Random Walk on Hirshfeld Surface

![Random Walk on Hirshfeld Surface](demo/HS_rdwk_test.mp4)
<!--<figure class="video_container">-->
<!--  <video height="240" controls>-->
<!--    <source src="demo/HS_rdwk_test.mp4" type="video/mp4">-->
<!--  </video>-->
<!--</figure>-->
